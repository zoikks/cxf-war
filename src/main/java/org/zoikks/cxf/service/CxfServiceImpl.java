package org.zoikks.cxf.service;

import org.zoikks.cxf.data.Patient;


public class CxfServiceImpl implements CxfService {

    @Override
    public Patient getPatient(String patientId) {

        Patient patient = new Patient();
        patient.setFirstName("Christina");
        patient.setLastName("Jones");

        return patient;
    }

    @Override
    public Patient putPatient(String patientId, Patient patient) {
        return null;
    }

    @Override
    public Patient postPatient(Patient patient) {
        return null;
    }
}
