package org.zoikks.cxf.service;

import org.apache.cxf.rs.security.cors.CrossOriginResourceSharing;
import org.zoikks.cxf.data.Patient;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@CrossOriginResourceSharing(
        allowAllOrigins = true
)
@Path("/")
@Produces({MediaType.APPLICATION_JSON})
@Consumes({MediaType.APPLICATION_JSON})
public interface CxfService {

    @GET
    @Path("/patients/{patientId}")
    Patient getPatient(@PathParam("patientId") String patientId);

    @PUT
    @Path("/patients/{patientId}")
    Patient putPatient(@PathParam("patientId") String patientId, Patient patient);

    @POST
    @Path("/patients")
    Patient postPatient(Patient patient);
}